#include "WiFi.h"
#include <WiFiClient.h>
#include <WebServer.h>
#include <DNSServer.h>

#define DEBUG_LOG_EN 1
#if DEBUG_LOG_EN
    #define DEBUG_LOG(a) Serial.println(a)
#else
    #define DEBUG_LOG(a) 
#endif


const byte DNS_PORT = 53;
DNSServer dnsServer;


/* Put IP Address details */
IPAddress local_ip(192,168,1,1);
IPAddress gateway(192,168,1,1);
IPAddress subnet(255,255,255,0);
WebServer server(80);
const char *ssid = "Co2SensorAP";
const char *password = "12345678";
// 9-bytes CMD PPM read command
const unsigned char mhzCmdReadPPM[9] = { 0xFF, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x79 };

const unsigned char charmhzCmdCalibrateZero[9] = { 0xFF, 0x01, 0x87, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x78 };
const unsigned char mhzCmdABCEnable[9] = { 0xFF, 0x01, 0x79, 0xA0, 0x00, 0x00, 0x00,
    0x00, 0xE6 };
const unsigned char mhzCmdABCDisable[9] = { 0xFF, 0x01, 0x79, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x86 };
const unsigned char mhzCmdReset[9] = { 0xFF, 0x01, 0x8d, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x72 };

const unsigned char mhzCmdMeasurementRange1000[9] = { 0xFF, 0x01, 0x99, 0x00, 0x00, 0x00,
    0x03, 0xE8, 0x7B };
const unsigned char mhzCmdMeasurementRange2000[9] = { 0xFF, 0x01, 0x99, 0x00, 0x00, 0x00,
    0x07, 0xD0, 0x8F };
const unsigned char mhzCmdMeasurementRange3000[9] = { 0xFF, 0x01, 0x99, 0x00, 0x00, 0x00,
    0x0B, 0xB8, 0xA3 };
const unsigned char mhzCmdMeasurementRange5000[9] = { 0xFF, 0x01, 0x99, 0x00, 0x00, 0x00,
    0x13, 0x88, 0xCB };


void init_co2_sensor(){
 Serial2.write(mhzCmdMeasurementRange2000, 9);
 delay(500);
 Serial2.write(mhzCmdABCEnable, 9);
  delay(500);
 Serial2.flush();
}

void reset(){
  Serial2.write(mhzCmdReset, 9);
}

int co2_read(int *ppm, int *temperature) {
  byte response[9]; // for answer
  Serial2.flush();
  Serial2.write(mhzCmdReadPPM, 9); //request PPM CO2

  // The serial stream can get out of sync. The response starts with 0xff, try to resync.
  while (Serial2.available() > 0
      && (unsigned char) Serial2.peek() != 0xFF) {
    Serial2.read();
  }

  memset(response, 0, 9);
  Serial2.readBytes(response, 9);

  if (response[1] != 0x86) {
    Serial.println("Invalid response from co2 sensor!");
    return -1;
  }

  byte crc = 0;
  for (int i = 1; i < 8; i++) {
    crc += response[i];
  }
  crc = 255 - crc + 1;

  if (response[8] == crc) {
    int responseHigh = (int) response[2];
    int responseLow = (int) response[3];
    int ppm_ = (256 * responseHigh) + responseLow;

    *ppm = ppm_;
    *temperature = (response[4] - 40);
     Serial.print("ppm = ");Serial.println(ppm_);
    
    return 1;

  } else {
    Serial.println("CRC error!");
    return -1;
  }
}
int ppm_co2=0;
int temperature=0;
void update_co2_async(int interval_ms){
    static unsigned long previousMillis = 0; 
    unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval_ms) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;

    int temp=0;
    int ppm=0;
    int res = co2_read(&ppm,&temp);
    if(res>0){
      ppm_co2=ppm;
      temperature=temp;
    }

}
}


String create_html_str(int ppm,int temp){
  String ptr = "<!DOCTYPE html> <html>\n";
  ptr +="<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  ptr +="<title>Co2 Sensor</title>\n";
  ptr +="<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n";
  ptr +="body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;}\n";
  ptr +="p {font-size: 24px;color: #444444;margin-bottom: 10px;}\n";
  ptr +="</style>\n";
  ptr +="</head>\n";
  ptr +="<body>\n";
  ptr +="<div id=\"webpage\">\n";
  ptr +="<h1>Co2 Sensor Report</h1>\n";
  
  ptr +="<p>Temperature: ";
  ptr +=(int)temp;
  ptr +=" oC </p>";
  ptr +="<p> Co2: ";
  ptr +=(int)ppm;
  ptr +=" ppm </p>";
  
  ptr +="</div>\n";

 // ptr += "<br> Go to <a href='config'>configure page</a> to change settings.";
  ptr +="</body>\n";
  ptr +="</html>\n";
  return ptr;
}


/** IP to String? */
String toStringIp(IPAddress ip)
{
  String res = "";
  for (int i = 0; i < 3; i++)
  {
    res += String((ip >> (8 * i)) & 0xFF) + ".";
  }
  res += String(((ip >> 8 * 3)) & 0xFF);
  return res;
}

boolean isIp(String str)
{
  for (size_t i = 0; i < str.length(); i++)
  {
    int c = str.charAt(i);
    if (c != '.' && (c < '0' || c > '9'))
    {
      return false;
    }
  }
  return true;
}



boolean handle_captive_portal()
{
  String host = server.hostHeader();
  DEBUG_LOG(host.c_str());
  if (!isIp(host))
  {
    DEBUG_LOG("Request redirected to captive portal");

    DEBUG_LOG("Request for ");
    DEBUG_LOG(host.c_str());
    DEBUG_LOG(" redirected to ");
    DEBUG_LOG(server.client().localIP());
    DEBUG_LOG("Redirect to : "+ String("http://") + toStringIp(server.client().localIP()));
    
    server.sendHeader("Location", String("http://") + toStringIp(server.client().localIP()), true);
    server.send(302, "text/plain", ""); // Empty content inhibits Content-length header so we have to close the socket ourselves.
    server.client().stop();             // Stop is needed because we sent no content length
    return true;
  }
  return false;
}



//===============================================================
// This routine is executed when you open its IP in browser
//===============================================================

void handle_not_found()
{
  if (handle_captive_portal())
  { // If captive portal redirect instead of displaying the error page.
    return;
  }
  else
  {
    String message = "File Not Found\n\n";
    message += "URI: ";
    message += server.uri();
    message += "\nMethod: ";
    message += (server.method() == HTTP_GET) ? "GET" : "POST";
    message += "\nArguments: ";
    message += server.args();
    message += "\n";
    for (uint8_t i = 0; i < server.args(); i++)
    {
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    server.send(404, "text/plain", message);
  }
}

void handleRoot() {

 //String s =create_html_str(ppm_co2,temperature);
 //server.send(200, "text/html", s); //Send web page

  if (handle_captive_portal())
  { // If captive portal redirect instead of displaying the error page.
    return;
  }
  else
  {
    //server.send(200, "text/html", web_portal);
    String s =create_html_str(ppm_co2,temperature);
    server.send(200, "text/html", s); //Send web page
  }
  
}

void handleSensor(){
 
}

void setup() {
 
  Serial.begin(115200);
  Serial2.begin(9600);
  delay(100);
  
  
  //WiFi.softAP(ssid, password);
  WiFi.softAP(ssid);

  WiFi.softAPConfig(local_ip, gateway, subnet);
  delay(100);
  Serial.println();
  Serial.print("IP address: ");
  Serial.println(WiFi.softAPIP());
  
  init_co2_sensor();
  
  server.on("/", handleRoot);      //This is display page
  server.on("/readSensor", handleSensor);//To get update of ADC Value only
  server.onNotFound([]() { handle_not_found(); }); //lamda C++ function type on C11
  // if DNSServer is started with "*" for domain name, it will reply with
  // provided IP to all DNS request
  dnsServer.start(DNS_PORT, "*", gateway);
  server.begin();
}
 
void loop() {
  update_co2_async(10000);//10sec
//  server.handleClient();
//  dnsServer.processNextRequest();

  dnsServer.processNextRequest();
  server.handleClient();
  //WiFiClient client = server.available();   // listen for incoming clients

  
  }
