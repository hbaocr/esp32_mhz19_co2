/*
  Multiple Serial test

  Receives from the main serial port, sends to the others.
  Receives from serial port 1, sends to the main serial (Serial 0).

  This example works only with boards with more than one serial like Arduino Mega, Due, Zero etc.

  The circuit:
  - any serial device attached to Serial port 1
  - Serial Monitor open on Serial port 0

  created 30 Dec 2008
  modified 20 May 2012
  by Tom Igoe & Jed Roach
  modified 27 Nov 2015
  by Arturo Guadalupi

  This example code is in the public domain.
*/
// 9-bytes CMD PPM read command
const unsigned char mhzCmdReadPPM[9] = { 0xFF, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x79 };

const unsigned char charmhzCmdCalibrateZero[9] = { 0xFF, 0x01, 0x87, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x78 };
const unsigned char mhzCmdABCEnable[9] = { 0xFF, 0x01, 0x79, 0xA0, 0x00, 0x00, 0x00,
    0x00, 0xE6 };
const unsigned char mhzCmdABCDisable[9] = { 0xFF, 0x01, 0x79, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x86 };
const unsigned char mhzCmdReset[9] = { 0xFF, 0x01, 0x8d, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x72 };

const unsigned char mhzCmdMeasurementRange1000[9] = { 0xFF, 0x01, 0x99, 0x00, 0x00, 0x00,
    0x03, 0xE8, 0x7B };
const unsigned char mhzCmdMeasurementRange2000[9] = { 0xFF, 0x01, 0x99, 0x00, 0x00, 0x00,
    0x07, 0xD0, 0x8F };
const unsigned char mhzCmdMeasurementRange3000[9] = { 0xFF, 0x01, 0x99, 0x00, 0x00, 0x00,
    0x0B, 0xB8, 0xA3 };
const unsigned char mhzCmdMeasurementRange5000[9] = { 0xFF, 0x01, 0x99, 0x00, 0x00, 0x00,
    0x13, 0x88, 0xCB };


void init_co2_sensor(){
 Serial2.write(mhzCmdMeasurementRange2000, 9);
 delay(500);
 Serial2.write(mhzCmdABCEnable, 9);
  delay(500);
 Serial2.flush();
}

void reset(){
  Serial2.write(mhzCmdReset, 9);
}

int co2_read(int *ppm, int *temperature) {
  byte response[9]; // for answer
  Serial2.flush();
  Serial2.write(mhzCmdReadPPM, 9); //request PPM CO2

  // The serial stream can get out of sync. The response starts with 0xff, try to resync.
  while (Serial2.available() > 0
      && (unsigned char) Serial2.peek() != 0xFF) {
    Serial2.read();
  }

  memset(response, 0, 9);
  Serial2.readBytes(response, 9);

  if (response[1] != 0x86) {
    Serial.println("Invalid response from co2 sensor!");
    return -1;
  }

  byte crc = 0;
  for (int i = 1; i < 8; i++) {
    crc += response[i];
  }
  crc = 255 - crc + 1;

  if (response[8] == crc) {
    int responseHigh = (int) response[2];
    int responseLow = (int) response[3];
    int ppm_ = (256 * responseHigh) + responseLow;

    *ppm = ppm_;
    *temperature = (response[4] - 40);
     Serial.print("ppm = ");Serial.println(ppm_);
    
    return 1;

  } else {
    Serial.println("CRC error!");
    return -1;
  }
}


void setup() {
  // initialize both serial ports:
  Serial.begin(115200);
  Serial2.begin(9600);

  init_co2_sensor();
  
}

void loop() {
  int temp=0;
  int ppm=0;
  int res = co2_read(&ppm,&temp);
  delay(5000);
}
