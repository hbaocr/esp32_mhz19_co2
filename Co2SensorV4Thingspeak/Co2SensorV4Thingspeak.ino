/*
  WriteMultipleFields
  
  Description: Writes values to fields 1,2,3,4 and status in a single ThingSpeak update every 20 seconds.
  
  Hardware: ESP32 based boards
  
  !!! IMPORTANT - Modify the secrets.h file for this project with your network connection and ThingSpeak channel details. !!!
  
  Note:
  - Requires installation of EPS32 core. See https://github.com/espressif/arduino-esp32/blob/master/docs/arduino-ide/boards_manager.md for details. 
  - Select the target hardware from the Tools->Board menu
  - This example is written for a network using WPA encryption. For WEP or WPA, change the WiFi.begin() call accordingly.
  
  ThingSpeak ( https://www.thingspeak.com ) is an analytic IoT platform service that allows you to aggregate, visualize, and 
  analyze live data streams in the cloud. Visit https://www.thingspeak.com to sign up for a free account and create a channel.  
  
  Documentation for the ThingSpeak Communication Library for Arduino is in the README.md folder where the library was installed.
  See https://www.mathworks.com/help/thingspeak/index.html for the full ThingSpeak documentation.
  
  For licensing information, see the accompanying license file.
  
  Copyright 2018, The MathWorks, Inc.
*/

#include "ThingSpeak.h"
//#include "secrets.h"
#include <WiFi.h>


// this is in secrete.h Use this file to store all of the private credentials 
// and connection details

#define SECRET_SSID "Tech_Reapra_2.4GHz"    // replace MySSID with your WiFi network name
#define SECRET_PASS "121nguyendu"  // replace MyPassword with your WiFi password

#define SECRET_CH_ID 780264     // replace 0000000 with your channel number
#define SECRET_WRITE_APIKEY "332T4V69XIFPSL6F"   // replace XYZ with your channel write API Key
//==========================

char ssid[] = SECRET_SSID;   // your network SSID (name) 
char pass[] = SECRET_PASS;   // your network password
int keyIndex = 0;            // your network key Index number (needed only for WEP)
WiFiClient  client;

unsigned long myChannelNumber = SECRET_CH_ID;
const char * myWriteAPIKey = SECRET_WRITE_APIKEY;

// Initialize our values
/*int number1 = 0;
int number2 = random(0,100);
int number3 = random(0,100);
int number4 = random(0,100);
String myStatus = "";
*/


// 9-bytes CMD PPM read command
const unsigned char mhzCmdReadPPM[9] = { 0xFF, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x79 };

const unsigned char charmhzCmdCalibrateZero[9] = { 0xFF, 0x01, 0x87, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x78 };
const unsigned char mhzCmdABCEnable[9] = { 0xFF, 0x01, 0x79, 0xA0, 0x00, 0x00, 0x00,
    0x00, 0xE6 };
const unsigned char mhzCmdABCDisable[9] = { 0xFF, 0x01, 0x79, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x86 };
const unsigned char mhzCmdReset[9] = { 0xFF, 0x01, 0x8d, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x72 };

const unsigned char mhzCmdMeasurementRange1000[9] = { 0xFF, 0x01, 0x99, 0x00, 0x00, 0x00,
    0x03, 0xE8, 0x7B };
const unsigned char mhzCmdMeasurementRange2000[9] = { 0xFF, 0x01, 0x99, 0x00, 0x00, 0x00,
    0x07, 0xD0, 0x8F };
const unsigned char mhzCmdMeasurementRange3000[9] = { 0xFF, 0x01, 0x99, 0x00, 0x00, 0x00,
    0x0B, 0xB8, 0xA3 };
const unsigned char mhzCmdMeasurementRange5000[9] = { 0xFF, 0x01, 0x99, 0x00, 0x00, 0x00,
    0x13, 0x88, 0xCB };


void init_co2_sensor(){
 Serial2.write(mhzCmdMeasurementRange2000, 9);
 delay(500);
 Serial2.write(mhzCmdABCEnable, 9);
  delay(500);
 Serial2.flush();
}

void reset(){
  Serial2.write(mhzCmdReset, 9);
}

int co2_read(int *ppm, int *temperature) {
  byte response[9]; // for answer
  Serial2.flush();
  Serial2.write(mhzCmdReadPPM, 9); //request PPM CO2

  // The serial stream can get out of sync. The response starts with 0xff, try to resync.
  while (Serial2.available() > 0
      && (unsigned char) Serial2.peek() != 0xFF) {
    Serial2.read();
  }

  memset(response, 0, 9);
  Serial2.readBytes(response, 9);

  if (response[1] != 0x86) {
    Serial.println("Invalid response from co2 sensor!");
    return -1;
  }

  byte crc = 0;
  for (int i = 1; i < 8; i++) {
    crc += response[i];
  }
  crc = 255 - crc + 1;

  if (response[8] == crc) {
    int responseHigh = (int) response[2];
    int responseLow = (int) response[3];
    int ppm_ = (256 * responseHigh) + responseLow;

    *ppm = ppm_;
    *temperature = (response[4] - 40);
     Serial.print("ppm = ");Serial.println(ppm_);
    
    return 1;

  } else {
    Serial.println("CRC error!");
    return -1;
  }
}


void setup() {
  Serial.begin(115200);
  Serial2.begin(9600);
  delay(500);
  init_co2_sensor();

  WiFi.mode(WIFI_STA);   
  ThingSpeak.begin(client);  // Initialize ThingSpeak
}

void loop() {

  // Connect or reconnect to WiFi
  if(WiFi.status() != WL_CONNECTED){
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(SECRET_SSID);
    while(WiFi.status() != WL_CONNECTED){
      WiFi.begin(ssid, pass);  // Connect to WPA/WPA2 network. Change this line if using open or WEP network
      Serial.print(".");
      delay(5000);     
    } 
    Serial.println("\nConnected.");
  }

  int temp=0;
  int ppm=0;
  int res = co2_read(&ppm,&temp);
  if(res>0){
     ThingSpeak.setField(1, ppm);
     ThingSpeak.setField(2, temp);
     ThingSpeak.setStatus("Read Sensor OK");
  }else{
    ThingSpeak.setStatus("Read Sensor Failed");
  }
 
  // write to the ThingSpeak channel
  int x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
  if(x == 200){
    Serial.println("Channel update successful.");
  }
  else{
    Serial.println("Problem updating channel. HTTP error code " + String(x));
  }
  
  delay(20000); // Wait 20 seconds to update the channel again
}
