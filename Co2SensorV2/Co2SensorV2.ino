/**
 * IotWebConf01Minimal.ino -- IotWebConf is an ESP8266/ESP32
 *   non blocking WiFi/AP web configuration library for Arduino.
 *   https://github.com/prampec/IotWebConf 
 *
 * Copyright (C) 2018 Balazs Kelemen <prampec+arduino@gmail.com>
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

/**
 * Example: Minimal
 * Description:
 *   This example will shows the bare minimum required for IotWeConf to start up.
 *   After starting up the thing, please search for WiFi access points e.g. with
 *   your phone. Use password provided in the code!
 *   After connecting to the access point the root page will automatically appears.
 *   We call this "captive portal".
 *   
 *   Please set a new password for the Thing (for the access point) as well as
 *   the SSID and password of your local WiFi. You cannot move on without these steps.
 *   
 *   You have to leave the access point before to let the Thing continue operation
 *   with connecting to configured WiFi.
 */

#include <DNSServer.h>
#include <IotWebConf.h>

// -- Initial name of the Thing. Used e.g. as SSID of the own Access Point.
const char thingName[] = "Co2Sensor";

// -- Initial password to connect to the Thing, when it creates an own Access Point.
const char wifiInitialApPassword[] = "12345678";

DNSServer dnsServer;
WebServer server(80);

IotWebConf iotWebConf(thingName, &dnsServer, &server, wifiInitialApPassword);

// 9-bytes CMD PPM read command
const unsigned char mhzCmdReadPPM[9] = { 0xFF, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x79 };

const unsigned char charmhzCmdCalibrateZero[9] = { 0xFF, 0x01, 0x87, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x78 };
const unsigned char mhzCmdABCEnable[9] = { 0xFF, 0x01, 0x79, 0xA0, 0x00, 0x00, 0x00,
    0x00, 0xE6 };
const unsigned char mhzCmdABCDisable[9] = { 0xFF, 0x01, 0x79, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x86 };
const unsigned char mhzCmdReset[9] = { 0xFF, 0x01, 0x8d, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x72 };

const unsigned char mhzCmdMeasurementRange1000[9] = { 0xFF, 0x01, 0x99, 0x00, 0x00, 0x00,
    0x03, 0xE8, 0x7B };
const unsigned char mhzCmdMeasurementRange2000[9] = { 0xFF, 0x01, 0x99, 0x00, 0x00, 0x00,
    0x07, 0xD0, 0x8F };
const unsigned char mhzCmdMeasurementRange3000[9] = { 0xFF, 0x01, 0x99, 0x00, 0x00, 0x00,
    0x0B, 0xB8, 0xA3 };
const unsigned char mhzCmdMeasurementRange5000[9] = { 0xFF, 0x01, 0x99, 0x00, 0x00, 0x00,
    0x13, 0x88, 0xCB };


void init_co2_sensor(){
 Serial2.write(mhzCmdMeasurementRange2000, 9);
 delay(500);
 Serial2.write(mhzCmdABCEnable, 9);
  delay(500);
 Serial2.flush();
}

void reset(){
  Serial2.write(mhzCmdReset, 9);
}

int co2_read(int *ppm, int *temperature) {
  byte response[9]; // for answer
  Serial2.flush();
  Serial2.write(mhzCmdReadPPM, 9); //request PPM CO2

  // The serial stream can get out of sync. The response starts with 0xff, try to resync.
  while (Serial2.available() > 0
      && (unsigned char) Serial2.peek() != 0xFF) {
    Serial2.read();
  }

  memset(response, 0, 9);
  Serial2.readBytes(response, 9);

  if (response[1] != 0x86) {
    Serial.println("Invalid response from co2 sensor!");
    return -1;
  }

  byte crc = 0;
  for (int i = 1; i < 8; i++) {
    crc += response[i];
  }
  crc = 255 - crc + 1;

  if (response[8] == crc) {
    int responseHigh = (int) response[2];
    int responseLow = (int) response[3];
    int ppm_ = (256 * responseHigh) + responseLow;

    *ppm = ppm_;
    *temperature = (response[4] - 40);
     Serial.print("ppm = ");Serial.println(ppm_);
    
    return 1;

  } else {
    Serial.println("CRC error!");
    return -1;
  }
}

void taskReadCo2( void * parameter)
{
    vTaskDelay(10000/portTICK_PERIOD_MS );
    int temp=0;
    int ppm=0;
    int res = co2_read(&ppm,&temp);
}

int ppm_co2=0;
int temperature=0;
void update_co2_async(int interval_ms){
    static unsigned long previousMillis = 0; 
    unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval_ms) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;

    int temp=0;
    int ppm=0;
    int res = co2_read(&ppm,&temp);
    if(res>0){
      ppm_co2=ppm;
      temperature=temp;
    }

}
}


String create_html_str(int ppm,int temp){
  String ptr = "<!DOCTYPE html> <html>\n";
  ptr +="<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  ptr +="<title>Co2 Sensor</title>\n";
  ptr +="<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n";
  ptr +="body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;}\n";
  ptr +="p {font-size: 24px;color: #444444;margin-bottom: 10px;}\n";
  ptr +="</style>\n";
  ptr +="</head>\n";
  ptr +="<body>\n";
  ptr +="<div id=\"webpage\">\n";
  ptr +="<h1>Co2 Sensor Report</h1>\n";
  
  ptr +="<p>Temperature: ";
  ptr +=(int)temp;
  ptr +=" oC </p>";
  ptr +="<p> Co2: ";
  ptr +=(int)ppm;
  ptr +=" ppm </p>";
  
  ptr +="</div>\n";

 // ptr += "<br> Go to <a href='config'>configure page</a> to change settings.";
  ptr +="</body>\n";
  ptr +="</html>\n";
  return ptr;
}



void setup() 
{
  Serial.begin(115200);

  // initialize both serial ports:
  Serial.begin(115200);
  Serial2.begin(9600);
  init_co2_sensor();
  
  Serial.println();
  Serial.println("Starting up...");

  // -- Initializing the configuration.
  iotWebConf.init();

  // -- Set up required URL handlers on the web server.
  server.on("/", handleRoot);
  server.on("/config", []{ iotWebConf.handleConfig(); });
  server.onNotFound([](){ iotWebConf.handleNotFound(); });
//   xTaskCreate(
//                     taskReadCo2,          /* Task function. */
//                     "ReadCo2",        /* String with name of task. */
//                     10000,            /* Stack size in bytes. */
//                     NULL,             /* Parameter passed as input of the task */
//                     1,                /* Priority of the task. */
//                     NULL);            /* Task handle. */
  Serial.println("Ready.");
}

void loop() 
{
  update_co2_async(10000);//10sec
  // -- doLoop should be called as frequently as possible.
  iotWebConf.doLoop();
}

/**
 * Handle web requests to "/" path.
 */
void handleRoot()
{
  // -- Let IotWebConf test and handle captive portal requests.
  if (iotWebConf.handleCaptivePortal())
  {
    // -- Captive portal request were already served.
    return;
  }
//   String s = "<!DOCTYPE html><html lang=\"en\"><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\"/>";
//   s += "<title>Co2 MHZ19 Sensor Config</title></head><body>Hello world!";
//   s += "Go to <a href='config'>configure page</a> to change settings.";
//   s += "</body></html>\n";

  String s =create_html_str(ppm_co2,temperature);
  s += "<br> Go to <a href='config'>configure page</a> to change settings.";
  server.send(200, "text/html", s);

}
